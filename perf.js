const {
	forLoop,
	whileAndForLoop,
	doubleWhileLoop
} = require('./index');

const testCases = [1, 2, 3, 10, 100, 1000, 10000, 100000];

testCases.forEach(rows => {
	console.log(`Testing with ${rows} rows:\n=====================`);
	console.time('forloop');
	forLoop(rows);
	console.timeEnd('forloop');

	console.time('whileAndForLoop');
	whileAndForLoop(rows);
	console.timeEnd('whileAndForLoop');

	console.time('doubleWhileLoop');
	doubleWhileLoop(rows);
	console.timeEnd('doubleWhileLoop');
	console.log('=====================');
});
