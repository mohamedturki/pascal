const {
	forLoop,
	whileAndForLoop,
	doubleWhileLoop
} = require('./index.js');

describe('PascalTringle', () => {
	it('returns correct a given row of a pascal triangle', () => {
		const testCases = [{
			rows: 0,
			expected: [1]
		}, {
			rows: 1,
			expected: [1, 1]
		}, {
			rows: 2,
			expected: [1, 2, 1]
		}, {
			rows: 3,
			expected: [1, 3, 3, 1]
		}, {
			rows: 5,
			expected: [1, 5, 10, 10, 5, 1]
		},{
			rows: 7,
			expected: [1, 7, 21, 35, 35, 21, 7, 1]
		}, {
			rows: 10,
			expected: [1, 10, 45, 120, 210, 252, 210, 120, 45, 10, 1]
		}];

		testCases.forEach((testCase) => {
			expect(forLoop(testCase.rows)).toEqual(testCase.expected);
			expect(whileAndForLoop(testCase.rows)).toEqual(testCase.expected);
			expect(doubleWhileLoop(testCase.rows)).toEqual(testCase.expected);
		});
	});
});