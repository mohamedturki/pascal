const forLoop = (rows = 0) => {
	let prev = [1];
	let next = [];

	for (let i = 0; i <= rows; i++) {
		for (let j = 0; j <= i; j++) {
			next.push((prev[j] || 0) + (prev[j - 1] || 0));
		}

		prev = next;
		next = [];
	}

	return prev;
}

const whileAndForLoop = (rows) => {
    let prev = [1];
    let next = [];
    let i = 0;

    while (i <= rows) {
        for (let j = 0; j <= i; j++ ) {
            next.push((prev[j] || 0) + (prev[j - 1] || 0));
        }
        prev = next;
        next = [];
        i++;
    }

    return prev;
}

const doubleWhileLoop = (rows) => {
    let prev = [1];
    let next = [];
    let i = 0;

    while (i <= rows) {
        let j = 0;
        while (j <= i) {
            next.push((prev[j] || 0) + (prev[j - 1] || 0));
            j++;
        }
        prev = next;
        next = [];
        i++;
    }

    return prev;
}


module.exports = {
	forLoop,
	whileAndForLoop,
	doubleWhileLoop
};
