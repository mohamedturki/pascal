Pascal Triangle
===============

Few implementations to generate Pascal's triangle.

Getting started & running tests
===============================

Clone the repo and then:
1. Install Node
2. `npm install --only=dev`
3. `npm run test` to run the tests
4. `npm run perf` to run the benchmarks